package me.lukasabc123abc.Zox;




import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

public class Zox_backbone extends JavaPlugin implements Listener{
	static Plugin plugin;
	List<String> commands = new ArrayList<String>();
	public void onDisable() 
	{
		plugin = null;
		Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&',
				"&8[&c&lZoX&8] &cShutting down &eZoneMC Zox plugin..."));
		
	}
	

	public void onEnable() {
		plugin = this;
		getServer().getPluginManager().registerEvents(this, this);
		Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&',
		"&8[&c&lZoX&8] &abooting up &eZoneMC Zox plugin..."));
		Bukkit.getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
		commands.add("/pl");
		commands.add("/plugin");
		commands.add("/bukkit:pl");
		commands.add("/bukkit:plugin");
		commands.add("/bukkit:version");
		commands.add("/bukkit:icanhasbukkit");
		commands.add("/version");
		commands.add("/icanhasbukkit");
	}
	@EventHandler
	public void onTab(PlayerJoinEvent event){
		Player player = event.getPlayer();
		if(player.hasPermission("tab.owner")){
		player.setPlayerListName(ChatColor.YELLOW + player.getName());
		}else if(player.hasPermission("tab.dev")){
		player.setPlayerListName(ChatColor.AQUA + player.getName());
		}else if(player.hasPermission("tab.vip")){
		player.setPlayerListName(ChatColor.DARK_PURPLE + player.getName());
		}else if(player.hasPermission("tab.mod")){
			player.setPlayerListName(ChatColor.RED + player.getName());
		}else if(player.hasPermission("tab.srmod")){
			player.setPlayerListName(ChatColor.DARK_RED + player.getName());
		}else{
			player.setPlayerListName(ChatColor.DARK_GRAY + player.getName());
		}
	}
	@EventHandler(priority = EventPriority.LOWEST)
	public void swear(AsyncPlayerChatEvent event){
		if(!event.getPlayer().hasPermission("zonemc.swear")){
		String[] swearWords = {"fuck", "pussy", "dick", "cock", "cunt", "kuk", "fitta", "snopp", "fml", "shit", "niglet", "nigger",  "wank", "bastard", "faggot", "fag", "this server sucks", "wtf"};
		
		for(String word: swearWords){
			 if(word == "wtf"){
				 event.setMessage(event.getMessage().replaceAll("(?i)" + word, "what the fruit"));
			  }else if(word.length() == 4){
		       event.setMessage(event.getMessage().replaceAll("(?i)" + word, "#@%&"));
			  }else if(word.length() == 5){
				  event.setMessage(event.getMessage().replaceAll("(?i)" + word, "#@%!&"));
			  }else if(word == "fml"){
				  event.setMessage(event.getMessage().replaceAll("(?i)" + word, "Forge mod loader"));
			  }else if(word.length() == 3){
				  event.setMessage(event.getMessage().replaceAll("(?i)" + word, "#&@"));
			  }else if(word == "this server sucks"){
				  event.setMessage(event.getMessage().replaceAll("(?i)" + word, "this is the best server ever"));
			  }else{
				  event.setMessage(event.getMessage().replaceAll("(?i)" + word, "#@%&$!"));
			  }
		}
	}
	}
	String prefix = ChatColor.translateAlternateColorCodes('&', "&8[&c&lZoX&8]");
	public int getPing(Player player) {
	    return ((CraftPlayer) player).getHandle().ping;
	}
	@EventHandler
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args){
        if(commandLabel.equalsIgnoreCase("ping")){
            	if(args.length <= 0){
            		if(sender.hasPermission("ping.pingself")){
            		if(sender instanceof Player){
            			if(getPing((Player) sender) < 120){
            				sender.sendMessage(prefix + ChatColor.GREEN + "Your ping is: "  + ChatColor.GREEN + "["+ ChatColor.GREEN + getPing((Player) sender) + ChatColor.GREEN + "]");
            			}else if(getPing((Player) sender) < 200){
            				sender.sendMessage(prefix + ChatColor.GREEN + "Your ping is: " + ChatColor.GREEN + "[" + ChatColor.RED + getPing((Player) sender) + ChatColor.GREEN + "]");
            			}
            	else{
            				sender.sendMessage(prefix + ChatColor.GREEN + "Your ping is: "  + ChatColor.GREEN + "["+ ChatColor.DARK_RED + getPing((Player) sender) + ChatColor.GREEN + "]");
            			}
            		}else{
            			sender.sendMessage(prefix + ChatColor.RED + "You must be a player to ping yourself you can use /ping {player} to ping an online player");
            		}
            	}else{
            		sender.sendMessage(prefix + ChatColor.RED + "You do not have permission");
            	}
            	}else if(args.length == 1){
            		if(sender.hasPermission("ping.pingothers")){
            		
            		if(getServer().getPlayer(args[0]) != null){
            			Player target = getServer().getPlayer(args[0]);
            			if(getPing((Player) target) < 120){
            				sender.sendMessage(prefix + ChatColor.GREEN + "Ping of "+ ChatColor.GREEN + target.getName() + ChatColor.GREEN + ":" + ChatColor.GREEN + "[" + ChatColor.GREEN + getPing((Player) target)+ ChatColor.GREEN + "]");
            			}else if(getPing((Player) target) < 200){
            				sender.sendMessage(prefix + ChatColor.GREEN + "Ping of "+ ChatColor.GREEN + target.getName() + ChatColor.GREEN + ":" + ChatColor.GREEN + "["+ ChatColor.RED + getPing((Player) target)+ ChatColor.GREEN + "]");
            			}
            			else{
            				sender.sendMessage(prefix + ChatColor.GREEN + "Ping of "+ ChatColor.GREEN + target.getName() + ChatColor.GREEN + ":" + ChatColor.GREEN + "[" + ChatColor.DARK_RED + getPing((Player) target)+ ChatColor.GREEN + "]");
            			}
            		}else{
            			sender.sendMessage(prefix + ChatColor.RED + "That player is offline");
            		}
            	}else{
            		sender.sendMessage(ChatColor.RED + "You do not have permission");
            	}
            	}else{
            		sender.sendMessage(prefix + ChatColor.RED + "Invalid argument use /ping or /ping {player}");
            	}
            	
            	
            }else if(commandLabel.equalsIgnoreCase("hub")){
            	if(sender instanceof Player){
            		if(!(Bukkit.getServerName() == "hub")){
            		Player p = (Player) sender;
            		ByteArrayDataOutput out = ByteStreams.newDataOutput();
            		out.writeUTF("Connect");
            		out.writeUTF("hub"); 
            		p.sendPluginMessage(plugin, "BungeeCord", out.toByteArray());
            		}else {
            			sender.sendMessage(prefix + ChatColor.translateAlternateColorCodes('&', " &cyou are allready in the hub server"));
            		}
            	}
            }
       

     
		return false;
}

	@EventHandler
	public void onCommandPreprocess(PlayerCommandPreprocessEvent event)
	{
		if(!event.getPlayer().hasPermission("zox.blocked")){
		String message = event.getMessage();
		String[] array = message.split(" ");
		for (int i = 0; i < commands.size(); i++) {
			if(array[0].equalsIgnoreCase(commands.get(i))){
				event.setCancelled(true);
				event.getPlayer().sendMessage(prefix + ChatColor.translateAlternateColorCodes('&', "&cYou do not have acces to view this page &2#BlameTheDev"));
			}
		}
		}
	}
	@EventHandler
	public void onjoin(PlayerJoinEvent e){
		Player player = e.getPlayer();
		if(!player.hasPlayedBefore()){
			e.setJoinMessage("");
	    }else {
	    	if(e.getPlayer().hasPermission("join.owner")){
	    	String message = "&e" + e.getPlayer().getName() + " &5Has joined the server!";
	    	e.setJoinMessage(ChatColor.translateAlternateColorCodes('&', message));
	    	}else if(e.getPlayer().hasPermission("join.dev")){
	    		String message = "&b" + e.getPlayer().getName() + " &5Has joined the server!";
		    	e.setJoinMessage(ChatColor.translateAlternateColorCodes('&', message));
	    	}else if(e.getPlayer().hasPermission("join.mod")){
	    		String message = "&c" + e.getPlayer().getName() + " &5Has joined the server!";
		    	e.setJoinMessage(ChatColor.translateAlternateColorCodes('&', message));
	    	}else if(e.getPlayer().hasPermission("join.srmod")){
	    		String message = "&4" + e.getPlayer().getName() + " &5Has joined the server!";
		    	e.setJoinMessage(ChatColor.translateAlternateColorCodes('&', message));
	    	}else{
	    		e.setJoinMessage("");
	    	}
	    }
	}
	@EventHandler
	public void onleave(PlayerQuitEvent e){
		e.setQuitMessage("");
	}
	

 
}
