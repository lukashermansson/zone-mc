package me.lukasabc123abc.ZoneFFA;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import code.husky.mysql.MySQL;

public class FFA extends JavaPlugin implements Listener{
	public static ArrayList<Player> ingame = new ArrayList<Player>();
	
	public static FFA plugin;
	//disabeling the plugin
	@SuppressWarnings("unused")
	@Override
	public void onDisable() {
		plugin = null;
		PluginDescriptionFile pdfFile = this.getDescription();
		Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&',
				"&8[&c&lZoX&8] &closing all threads on &eZoneFFA plugin..."));
		try {
			c.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	//enableling
	@Override
	public void onEnable() {
		registerEvents(this, new SignManager());
		Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&',
		"&8[&c&lZoX&8] &abooting up &eZoneFFA plugin..."));
		plugin = this;
		getServer().getPluginManager().registerEvents(this, this);
		SignManager.setup();
		try {
			c = MySQL.openConnection();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	static MySQL MySQL = new MySQL(plugin, "localhost", "3306", "InvMode", "pexuser", "pex");
	static Connection c = null;
	//util for register event
	public static void registerEvents(org.bukkit.plugin.Plugin plugin, Listener... listeners) {
		for (Listener listener : listeners) {
			Bukkit.getServer().getPluginManager().registerEvents(listener, plugin);
		}
	}
	@EventHandler
	public void ondeth(PlayerDeathEvent event){
		event.setDeathMessage("");
		// clear the drops
	
		event.getDrops().clear();
		if(event.getEntity().getKiller() instanceof Player){
			Player player = event.getEntity().getKiller();
			//don't care just a dual method 
			event.getEntity().sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&cZoneFFA&8] &2" + player.getName() + " killed you with Hp left!"));
			player.addPotionEffect(new PotionEffect(PotionEffectType.ABSORPTION, 0, 0),true);
		}
	}
	@EventHandler(priority = EventPriority.HIGHEST)
	 public void onPlayerChatEvent(AsyncPlayerChatEvent event){
		 event.setFormat(event.getFormat().replace("Ipoint", ""));
	}
	@EventHandler
	public void onfooddecay(FoodLevelChangeEvent event){
		event.setCancelled(true);
	}
	@EventHandler
	public void onPlayerDropItem(PlayerDropItemEvent event) {
	 
		if(!event.getPlayer().hasPermission("zonemc.drop")){
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onleave(PlayerQuitEvent event){
		if(ingame.contains(event.getPlayer())){
			ingame.remove(event.getPlayer());
		}
		event.getPlayer().getInventory().clear();
		event.getPlayer().getInventory().setArmorContents(null);
	}
	public static void addingame(Player p){
	if(!ingame.contains(p)){
		ingame.add(p);
	}
	}
	@EventHandler
	public void onjoin(PlayerJoinEvent event){
		World w = Bukkit.getWorld("world");
		event.getPlayer().teleport(w.getSpawnLocation());
	}
}
