package me.lukasabc123abc.ZoneFFA;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class SignManager implements Listener{
	
	static ArrayList<Location> arena1 = new ArrayList<Location>();
	static ArrayList<Location> arena2 = new ArrayList<Location>();
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event)
    {
        if(event.getAction().equals(Action.RIGHT_CLICK_BLOCK))
        {
            Block b = event.getClickedBlock();
            if(b.getState() instanceof Sign)
            {
                Sign sign = (Sign) b.getState();
                final Player player = event.getPlayer();
                if(sign.getLine(1).equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', "&8[&5Join&8]"))){
                	if(sign.getLine(2).equalsIgnoreCase("map1")){
                		 Bukkit.getScheduler().runTaskLater(FFA.plugin, new Runnable() {
         			        @Override
         			        public void run() {
         			        	stackplayer(player);
                        		randomtp(1, player);
         			        }
         			    }, 1);
                		FFA.addingame(player);
                	}else if(sign.getLine(2).equalsIgnoreCase("map2")){
                		
                	}else if(sign.getLine(2).equalsIgnoreCase("map3")){
                		
                	}else{
                		player.sendMessage(ChatColor.translateAlternateColorCodes('&',
                		"&8[&c&lZoX&8] &cNot a map name"));
                	}
                }else{
                	
                }
                event.setCancelled(true);
            }
        }
    }

	private void randomtp(int i, final Player player) {
		if(i == 1){
			Random random = new Random();
			int randomloc = random.nextInt(arena1.size());
			Location loc = arena1.get(randomloc);
		
			player.teleport(loc);
			 Bukkit.getScheduler().runTaskLater(FFA.plugin, new Runnable() {
			        @Override
			        public void run() {
			        	player.teleport(player);
			        }
			    }, 5);
		}else if (i == 2){
			Random random = new Random();
			int randomloc = random.nextInt(arena2.size());
			Location loc = arena2.get(randomloc);
			
			player.teleport(loc);
		}
	}
	public void stackplayer(Player p) {
		ItemStack Helm = new ItemStack(Material.IRON_HELMET);
		p.getInventory().setHelmet(Helm);
		ItemStack chest = new ItemStack(Material.IRON_CHESTPLATE);
		p.getInventory().setChestplate(chest);
		ItemStack legs = new ItemStack(Material.IRON_LEGGINGS);
		p.getInventory().setLeggings(legs);
		ItemStack boots = new ItemStack(Material.IRON_BOOTS);
		p.getInventory().setBoots(boots);
		
		ItemStack sword = new ItemStack(Material.IRON_SWORD);
		ItemStack rod = new ItemStack(Material.FISHING_ROD);
		ItemStack bow = new ItemStack(Material.BOW);
		ItemStack arrows = new ItemStack(Material.ARROW, 10);
		
		Inventory inv = p.getInventory();
		try {
			if(invmode(p) == 1){
				inv.setItem(0, sword);
				inv.setItem(8, rod);
				inv.setItem(1, bow);
				inv.setItem(2, arrows);
			}else if(invmode(p) == 2){
				inv.setItem(0, sword);
				inv.setItem(1, rod);
				inv.setItem(8, bow);
				inv.setItem(7, arrows);
			}else{
				inv.setItem(0, sword);
				inv.setItem(1, rod);
				inv.setItem(2, bow);
				inv.setItem(3, arrows);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public static void setup() {
		World world = FFA.plugin.getServer().getWorld("ffa");
		arena1.add(new Location(world, 3, 32, 23));
		arena1.add(new Location(world, -17, 27, -22));
		arena1.add(new Location(world, 60, 25, 9));
		arena1.add(new Location(world, 15, 28, -48));
		arena1.add(new Location(world, -13, 20, -49));
		arena1.add(new Location(world, -4, 26, -7));
		arena1.add(new Location(world, -43, 21, -13));
		arena1.add(new Location(world, -57, 30, 29));
		arena1.add(new Location(world, 1, 40, 63));
		arena1.add(new Location(world, 47, 36, 39));
		arena1.add(new Location(world, -7, 46, 67));
	}
	@EventHandler
	public void onrespawn(final PlayerRespawnEvent event){
		if(FFA.ingame.contains(event.getPlayer())){
			 Bukkit.getScheduler().runTaskLater(FFA.plugin, new Runnable() {
			        @Override
			        public void run() {
			    		stackplayer(event.getPlayer());
			    		randomtp(1, event.getPlayer());
			        }
			    }, 1);
		}
	}
	@SuppressWarnings("deprecation")
	@EventHandler
	public void ontp(PlayerTeleportEvent event){
		for (Player player : Bukkit.getOnlinePlayers()) {
			player.hidePlayer(event.getPlayer());
			}
			for (Player player : Bukkit.getOnlinePlayers()) {
			player.showPlayer(event.getPlayer());
			}
	}
	public int invmode(Player p) throws SQLException{
		if(FFA.c.isClosed()){
			try {
				FFA.c = FFA.MySQL.openConnection();
			} catch (ClassNotFoundException e) {
				
				e.printStackTrace();
			}
		}
		Statement statement = FFA.c.createStatement();
		ResultSet res = statement.executeQuery("SELECT * FROM Modes WHERE name = '" + p.getName() + "';");
		if(res.next() == false){
			return 0;
		}else{
			return res.getInt("mode");
		}
		 
	}
	

}
