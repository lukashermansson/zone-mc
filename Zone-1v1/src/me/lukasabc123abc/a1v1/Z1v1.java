package me.lukasabc123abc.a1v1;


import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


import java.util.IllegalFormatException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import code.husky.mysql.MySQL;

public class Z1v1 extends JavaPlugin implements Listener{

		static Plugin plugin;
		MySQL MySQL = new MySQL(plugin, "localhost", "3306", "1v1stats", "pexuser", "pex");
		Connection c = null;
		public void onDisable() 
		{
			plugin = null;
			Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&',
					"&8[&c&lZoX&8] &cShutting down &eZoneMC 1v1 plugin..."));
			c = null;
		}
		

		@Override
		public void onEnable() {
			plugin = this;
			getServer().getPluginManager().registerEvents(this, this);
			Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&',
			"&8[&c&lZoX&8] &abooting up &eZoneMC 1v1 plugin..."));
			try {
				c = MySQL.openConnection();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}    
		
		public static Plugin getPlugin() {
			return plugin;
		}


		public void registerEvents(org.bukkit.plugin.Plugin plugin, Listener... listeners) {
			for (Listener listener : listeners) {
			Bukkit.getServer().getPluginManager().registerEvents(listener, plugin);
			}
		}

		public String points(Player p) throws SQLException, ClassNotFoundException{
			if(c.isClosed()){
				c = MySQL.openConnection();
			}
			Statement statement = c.createStatement();
			ResultSet res = statement.executeQuery("SELECT points FROM stats WHERE name = '" + p.getName() + "';");
			if(res.next() == false){
				return "empty";
			}else{
				return res.getInt("points") + "";
			}
			
		}

		
		 @EventHandler(priority = EventPriority.HIGHEST)
		 public void onPlayerChatEvent(AsyncPlayerChatEvent event){
			 try {
					 event.setFormat(event.getFormat().replace("Ipoint", ChatColor.translateAlternateColorCodes('&', "&8▏&c&l" + points(event.getPlayer()) + "&8▏&r ")));
			} catch (IllegalFormatException | NullPointerException
					| SQLException | ClassNotFoundException e) {
				e.printStackTrace();
			}
		 }
		 @EventHandler
		 public void OnjoinGame(PlayerJoinEvent event) throws SQLException, ClassNotFoundException{
			 if(c.isClosed()){
					c = MySQL.openConnection();
				}
			 Statement statement1 = c.createStatement();
				ResultSet res1 = statement1.executeQuery("SELECT name FROM stats WHERE name = '" + event.getPlayer().getName() + "';");
				if(res1.next() == false){
						Statement statement = c.createStatement();
						statement.executeUpdate("INSERT INTO stats(name, uuid) VALUES('" + event.getPlayer().getName() + "', '" + event.getPlayer().getUniqueId() +"')");
						
				}
		 }
		 @SuppressWarnings("unused")
		@EventHandler
			public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args){
		      if(commandLabel.equalsIgnoreCase("stats")){
		            if(!sender.hasPermission("zonemc.stats")) {
		                sender.sendMessage(ChatColor.RED + " You Don't Have Permission!");
		                return true;
		            }
		            if(args.length > 0){
		            String target = args[0];
					if(target != null) {
						
						try {
							if(c.isClosed()){
								c = MySQL.openConnection();
							}
							Statement statement1;
							statement1 = c.createStatement();
							ResultSet res1 = statement1.executeQuery("SELECT points, wins, losses, ties FROM stats WHERE name = '" + target + "';");
							Statement statement2;
							statement2 = c.createStatement();
							ResultSet res2 = statement2.executeQuery("SELECT COUNT(*) FROM stats WHERE points >= (SELECT points FROM stats WHERE name='" + target + "');");
							if(res1.next() == false){
								 sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&c&lZoX&8] &cPlayer has never logged on!"));
							}else{
								Statement statement3;
								statement3 = c.createStatement();
								ResultSet res3 = statement3.executeQuery("SELECT COUNT(*) FROM stats WHERE points = '" + res1.getInt("points") + "';");
								res2.next();
								res3.next();
								int playerwithsame = res3.getInt("COUNT(*)") - 1;
								int rank = res2.getInt("COUNT(*)") - playerwithsame;
					        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&eZoneMC&8] &bStats of " + target));
							sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&eZoneMC&8] &ePoints: &c" + res1.getInt("points")));
							sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&eZoneMC&8] &eRanked 1v1 Stats : &a" + res1.getInt("wins")+ "&8/&6" + res1.getInt("ties") + "&8/&4" + res1.getInt("losses")));
							sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&eZoneMC&8] &eglobal ranking: #" + rank));
							}
						} catch (SQLException e) {
							e.printStackTrace();
						} catch (ClassNotFoundException e) {
							e.printStackTrace();
						}
						}
		            }else{
		            	//load player config
				        File file = new File("plugins"+File.separator+"players"+File.separator+ sender.getName()+".yml");
				        FileConfiguration config1 = YamlConfiguration.loadConfiguration(file);
				        if(sender instanceof Player){
						
						try {
							if(c.isClosed()){
								c = MySQL.openConnection();
							}
							Statement statement1;
							statement1 = c.createStatement();
							ResultSet res1 = statement1.executeQuery("SELECT points, wins, losses, ties FROM stats WHERE name = '" + sender.getName() + "';");
							Statement statement2;
							statement2 = c.createStatement();
							ResultSet res2 = statement2.executeQuery("SELECT COUNT(*) FROM stats WHERE points >= (SELECT points FROM stats WHERE name='" + sender.getName() + "');");
							if(res1.next() == false){
								 sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&cZoX&8] &cyou have never been on strange?? conatact a dev imidiatly"));
							}else{
								Statement statement3;
								statement3 = c.createStatement();
								ResultSet res3 = statement3.executeQuery("SELECT COUNT(*) FROM stats WHERE points = '" + res1.getInt("points") + "';");
								res2.next();
								res3.next();
								int playerwithsame = res3.getInt("COUNT(*)") - 1;
								int rank = res2.getInt("COUNT(*)") - playerwithsame;
					        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&eZoneMC&8] &bStats of " + sender.getName()));
							sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&eZoneMC&8] &ePoints: &c" + res1.getInt("points")));
							sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&eZoneMC&8] &eRanked 1v1 Stats : &a" + res1.getInt("wins")+ "&8/&6" + res1.getInt("ties") + "&8/&4" + res1.getInt("losses")));
							sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&eZoneMC&8] &eglobal ranking: #" + rank));
							}
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (ClassNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						}else{
							sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&c&lZoX&8] &4console cant have stats"));
						}
		      }
		      }else if(commandLabel.equalsIgnoreCase("leaderboard") || commandLabel.equalsIgnoreCase("top")){
				  try {
					  if(c == null){
							c = MySQL.openConnection();
						}
					  sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&eZoneMC&8] &bLeaderboard for 1v1!"));
					  Statement statement1;
			    	  statement1 = c.createStatement();
			    	  ResultSet res1 = statement1.executeQuery("SELECT name, points FROM stats ORDER BY points DESC LIMIT 10;");
			    	  int numb = 1;
			    	  while (res1.next()){
						sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&c-&8] &2"+ numb + ". &8[&c&l"+ res1.getInt("points") + "&8] &c" + res1.getString("name")));
						numb++;
			    	  }
				} catch (SQLException e) {
					e.printStackTrace();
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
		      }
			return false;
}
}
