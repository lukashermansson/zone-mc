package me.lukasabc123abc.a1v1;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.plugin.Plugin;

public class Arena implements Listener{
	final int warmupp;
	int arenatimer;
	Player pl1;
	Player pl2;
	int arena;
	int stage;
	int pl1points;
	int pl2points;
	public Arena(Player player1, Player player2, int arenaint, final Plugin pl) {
	    pl1 = player1;
	    pl2 = player2;
	    pl1points = 0;
	    pl2points = 0;
	    arena = arenaint;
	    stage = 0;
	    // 0 = pregame
	    // 1 = first round
	    // 2 second round
	    // 3 third round
	    //teleport into arena
	    
	    
	    
	    warmupp = pl.getServer().getScheduler().scheduleSyncRepeatingTask(pl, new Runnable() {
        int timer = 5;
        public void run() {
        	timer --;
            if(timer <= 0){
            	startarena(pl);
            }
        }
      }, 0L, 20L);
        // 60 L == 3 sec, 20 ticks == 1 sec
	    //pregame
	    
	    
	    
	}

	public void startarena(Plugin pl) {
		Bukkit.getScheduler().cancelTask(warmupp);
		round(pl);
	}
	
	public void round(Plugin pl){
	    arenatimer = pl.getServer().getScheduler().scheduleSyncRepeatingTask(pl, new Runnable() {
	        int timer = 3*60;
	        public void run() {
	        	timer --;
	            if(timer <= 0){
	            	newround();
	            }
	        }
	      }, 0L, 20L);
	        // 60 L == 3 sec, 20 ticks == 1 sec
		    //pregame
		    
	}
	protected void newround() {
		Bukkit.getScheduler().cancelTask(arenatimer);
		stage++;
		if(stage >= 3){
			if(pl1points > 2 && pl2points > 2){
				round(Z1v1.getPlugin());
			}else{
				endgame();
			}
		}
		
	}

	private void endgame() {
		if(pl1points == 2){
			Bukkit.getServer().broadcastMessage(pl1.getName() + " won");
		}else if(pl2points == 2){
			Bukkit.getServer().broadcastMessage(pl2.getName() + " won");
		}else{
			//both won?
		}
		
	}

	@EventHandler
	public void onplayerdeth(PlayerDeathEvent event){
        Player dead = event.getEntity();
        Player killer = dead.getKiller();
        if(killer instanceof Player){
        	if(killer.getName() == pl1.getName() || killer.getName() == pl2.getName()){
        		
        		if(killer.getName() == pl1.getName()){
        			pl1points++;
        		}else{
        			pl2points++;
        		}
        		newround();
        		}
        	}
        }
}
