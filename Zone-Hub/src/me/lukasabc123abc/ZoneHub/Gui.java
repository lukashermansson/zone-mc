package me.lukasabc123abc.ZoneHub;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;


	public class Gui implements Listener{

		 
		public static ItemStack a1v1(){
			ItemStack a1v1 = new ItemStack(Material.IRON_SWORD);
			//define your itemmeta here
	    	List<String> Lore = new ArrayList<String>();
	    	ItemMeta im = a1v1.getItemMeta();
	    	im.setDisplayName(ChatColor.RED + "ffa");
	    	Lore.add(ChatColor.AQUA + "pvp ageans large amount of players");
	    	Lore.add(ChatColor.AQUA + "good for training!");
	    	im.setLore(Lore);
	    	a1v1.setItemMeta(im);
			 
			//return the item

			return a1v1;
			 
			}
		  public static ItemStack Buy(){
			ItemStack Buy = new ItemStack(Material.EMERALD_BLOCK);
			//define your itemmeta here
	    	List<String> Lore = new ArrayList<String>();
	    	ItemMeta im = Buy.getItemMeta();
	    	im.setDisplayName(ChatColor.GOLD + "Buy");
	    	Lore.add(ChatColor.AQUA + "Click here to view all server ranks");
	    	im.setLore(Lore);
	    	Buy.setItemMeta(im);
			 
			//return the item

			return Buy;
			 
			}
		    public static void openpotGUI(Player p, int rows)
		    {
			    //make inv
		        Inventory gamemenu = Bukkit.createInventory(p, rows*9,ChatColor.LIGHT_PURPLE + "Game menu");
		        gamemenu.setItem(0, Gui.a1v1());
		        gamemenu.setItem(8, Gui.Buy());
		        p.openInventory(gamemenu);
		    }
		    @EventHandler
		    public void onClick(InventoryClickEvent e)
		    {
		        if(e.getInventory().getTitle().equalsIgnoreCase(ChatColor.LIGHT_PURPLE + "Game menu"))
		        {
		            if(e.getCurrentItem() != null && e.getCurrentItem().getType() != null)
		            {
		                e.setCancelled(true);
		                if(e.getCurrentItem().hasItemMeta() && e.getCurrentItem().getItemMeta().hasDisplayName())
		                {
		                    if(e.getCurrentItem().getItemMeta().getDisplayName().endsWith("ffa")){
		                    	Player player = (Player) e.getWhoClicked();
		                    	ByteArrayDataOutput out = ByteStreams.newDataOutput();
		                        out.writeUTF("Connect");
		                        out.writeUTF("ffa"); 
		                      player.sendPluginMessage(Main.plugin, "BungeeCord", out.toByteArray());
		                    }else if(e.getCurrentItem().getItemMeta().getDisplayName().endsWith("Buy")){
		                    	((Player) e.getWhoClicked()).chat("/buy");
		                    }
		                }
		            }
		        }
		    }
}
