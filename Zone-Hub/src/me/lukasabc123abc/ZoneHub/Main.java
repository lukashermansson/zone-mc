package me.lukasabc123abc.ZoneHub;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener{
	
	static Plugin plugin;
	public void onDisable() 
	{
		plugin = null;
		Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&',
				"&8[&c&lZoX&8] &cShutting down &eZoneMC hub plugin..."));
		
	}
	

	@Override
	public void onEnable() {
		plugin = this;
		registerEvents(this, new Gui(), new Chat());
		getServer().getPluginManager().registerEvents(this, this);
		Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&',
		"&8[&c&lZoX&8] &abooting up &eZoneMC hub plugin..."));
		getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
	}    
	
	public static void registerEvents(org.bukkit.plugin.Plugin plugin, Listener... listeners) {
		for (Listener listener : listeners) {
		Bukkit.getServer().getPluginManager().registerEvents(listener, plugin);
		}
		}



	@EventHandler
	public void rightclick(PlayerInteractEvent event){
		if(event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK){
			if(!(event.getPlayer().getItemInHand() == null)){
				if(event.getPlayer().getItemInHand().getType() == Material.COMPASS){
					Gui.openpotGUI(event.getPlayer(), 1);
				}
			}
		}
	}
	
	public static ItemStack compass(){
		ItemStack compass = new ItemStack(Material.COMPASS);
		//define your itemmeta here
    	List<String> Lore = new ArrayList<String>();
    	ItemMeta im = compass.getItemMeta();
    	im.setDisplayName("" + ChatColor.YELLOW + ChatColor.BOLD + "Game Menu");
    	Lore.add(ChatColor.AQUA + "Right click to view all gamemodes");
      	im.setLore(Lore);
    	compass.setItemMeta(im);
		 
		//return the item

		return compass;
		 
		}
	@EventHandler
	public void onTab(PlayerJoinEvent event) throws IOException{
		Player player = event.getPlayer();
		
		
		
		player.getInventory().setItem(0, compass());
		
		ItemStack book = new ItemStack(Material.WRITTEN_BOOK);
		BookMeta bm = (BookMeta) book.getItemMeta();
		bm.setAuthor(ChatColor.translateAlternateColorCodes('&', "&eZoneMC"));
		bm.addPage();
		bm.setTitle(ChatColor.translateAlternateColorCodes('&', "&e&lZoneMC Guide"));
		book.setItemMeta(bm);
		player.getInventory().setItem(1, book);
		
		World w = Bukkit.getWorld("hubv2");
		player.teleport(w.getSpawnLocation());
		
		if(!player.hasPermission("Zonemc.adventure")){
			player.setGameMode(GameMode.ADVENTURE);
		}
	}


	public static Plugin getPlugin() {
		return plugin;
	}
	
	@EventHandler
	public void onfooddecay(FoodLevelChangeEvent event){
		event.setCancelled(true);
	}
	@EventHandler
	public void Onpickup(PlayerPickupItemEvent event){
		if(!event.getPlayer().hasPermission("zonemc.pickup")){
			event.setCancelled(true);
		}
	}
	@EventHandler
	public void onPlayerDropItem(PlayerDropItemEvent event) {
	 
		if(!event.getPlayer().hasPermission("zonemc.drop")){
			event.setCancelled(true);
		}
	}
	@EventHandler
	public void onInventoryClickEvent(InventoryClickEvent event){
	 
		if(!event.getWhoClicked().hasPermission("zonemc.drag")){
			event.setCancelled(true);
		}
	}
	 @EventHandler(priority = EventPriority.HIGHEST)
	 public void onPlayerChatEvent(AsyncPlayerChatEvent event){
		 event.setFormat(event.getFormat().replace("Ipoint", ""));
	 }
	

	
}
