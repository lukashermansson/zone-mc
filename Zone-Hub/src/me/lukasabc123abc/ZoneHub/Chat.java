package me.lukasabc123abc.ZoneHub;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;

public class Chat implements Listener, CommandExecutor{
	String mode = "open";

	@EventHandler
	public void onchat(AsyncPlayerChatEvent event){
		if(mode == "open"){
			
		}else if(mode == "restricted"){
			if(!event.getPlayer().hasPermission("Chat.res")){
				event.setCancelled(true);
				String message = "&e[&c&lZoX&e] &4Chat is in restricted mode only staff may speak!";
				event.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', message));
			}
		}else if(mode == "Muted"){
			if(!event.getPlayer().hasPermission("Chat.muted")){
				event.setCancelled(true);
				String message = "&e[&c&lZoX&e] &4Chat is in muted mode only &4srmods&8/&bDevs&8/&eOwners may speak!";
				event.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', message));
		}
		}
	}
	@EventHandler
	public void onjoin(PlayerJoinEvent event){
		if(mode == "open"){
			
		}else if(mode == "restricted"){
			String message = "&e[&c&lZoX&e] &aChat is in restricted mode";
			event.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', message));
		}else if(mode == "Muted"){
			String message = "&e[&c&lZoX&e] &4Chat is in Muted mode";
			event.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', message));
		}
	}
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args){
		if(commandLabel.equalsIgnoreCase("chat")){
			if(sender.hasPermission("chat.mode")){
			if(args.length >= 2){
			if(args[0].equalsIgnoreCase("mode")){
				if(args[1].equalsIgnoreCase("open")){
					mode = "open";
					String message = "&e[&c&lZoX&e] &aYou put the chat in open mode";
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
					String broadcast = "&e[&c&lZoX&e] &aChat just enterd Open mode";
					Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', broadcast));
				}else if(args[1].equalsIgnoreCase("res")){
					mode = "restricted";
					String message = "&e[&c&lZoX&e] &cYou put the chat in restriced mode";
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
					String broadcast = "&e[&c&lZoX&e] &cChat just enterd restricted mode";
					Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', broadcast));
				}else if(args[1].equalsIgnoreCase("muted")){
					mode = "Muted";
					String message = "&e[&c&lZoX&e] &4You put the chat in muted mode";
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
					String broadcast = "&e[&c&lZoX&e] &4Chat just enterd Muted mode";
					Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', broadcast));
				}else{
					String message = "&e[&c&lZoX&e] &cnot a valid option options are: open/res/muted";
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
				}
			}else{
				String message = "&e[&c&lZoX&e] &cSyntax: /chat mode open/res/muted";
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
			}
			}else{
				String message = "&e[&c&lZoX&e] &cSyntax: /chat mode open/res/muted";
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
			}
		}else{
			String message = "&e[&c&lZoX&e] &4You dont have acces to this command";
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&', message));
		}	
	}
		return false;
	}
}
