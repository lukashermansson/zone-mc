package me.lukasabc123abc.zfactions;



import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener{
	static Plugin plugin;

	public void onDisable() 
	{
		plugin = null;
		Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&',
				"&8[&c&lZoX&8] &cShutting down &eZoneMC faction plugin..."));
		
	}
	

	@Override
	public void onEnable() {
		plugin = this;
		getServer().getPluginManager().registerEvents(this, this);
		Bukkit.getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&',
		"&8[&c&lZoX&8] &abooting up &eZoneMC faction plugin..."));
		registerEvents(this, new Gui());
	}    
	
	public static void registerEvents(org.bukkit.plugin.Plugin plugin, Listener... listeners) {
		for (Listener listener : listeners) {
		Bukkit.getServer().getPluginManager().registerEvents(listener, plugin);
		}
		}



	
	 @EventHandler(priority = EventPriority.HIGHEST)
	 public void onPlayerChatEvent(AsyncPlayerChatEvent event){
		 event.setFormat(event.getFormat().replace("Ipoint", ChatColor.translateAlternateColorCodes('&', "")));
	 }
	

	
}
